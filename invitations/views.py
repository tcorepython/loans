import json
import datetime

from django.conf import settings
from django.contrib.sites.models import Site
try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from rest_framework import serializers
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import Http404, HttpResponse
from rest_framework.mixins import (
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin,
    DestroyModelMixin
)
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView, View
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth import get_user_model
from rest_framework.decorators import api_view, permission_classes

from .adapters import get_invitations_adapter
from .app_settings import app_settings
from .exceptions import AlreadyAccepted, AlreadyInvited, UserRegisteredEmail
from .forms import CleanEmailMixin
from .serializers import SendInviteSerializer, SendInvestorInviteSerializer
from .signals import invite_accepted
from .utils import get_invitation_model, get_invite_form
from .models import Invitation, InvestorInvitation
from borrowers.models import *
from investors.models import *
from accounts.models import *


class SendInviteViewSet(GenericViewSet,  # generic view functionality
                     CreateModelMixin,  # handles POSTs
                     RetrieveModelMixin,  # handles GETs for 1 invite
                     UpdateModelMixin,  # handles PUTs and PATCHes
                     DestroyModelMixin, # handles DELETE for 1 invite
                     ListModelMixin):  # handles GETs for many invites
    serializer_class = SendInviteSerializer
    queryset = Invitation.objects.all()


class SendInvestorInviteViewSet(GenericViewSet,  # generic view functionality
                     CreateModelMixin,  # handles POSTs
                     RetrieveModelMixin,  # handles GETs for 1 invite
                     UpdateModelMixin,  # handles PUTs and PATCHes
                     DestroyModelMixin, # handles DELETE for 1 invite
                     ListModelMixin):  # handles GETs for many invites
    serializer_class = SendInvestorInviteSerializer
    queryset = InvestorInvitation.objects.all()


# class AcceptInviteViewSet(GenericViewSet,  # handles GETs for 1 invit
#                      UpdateModelMixin,  # handles PUTs and PATCHes
#                      ListModelMixin):  # handles GETs for many invites
#     serializer_class = AcceptInviteSerializer
#     lookup_field = 'key'
#     queryset = Invitation.objects.all()

#     def partial_update(self, request, key=None):
#         instance = self.get_object(key)
#         serializer = self.serialize(instance, data=request.data, partial=True)
#         serializer.is_valid(raise_exception=True)
#         new_instance = serializer.save()
#         return Response(serializer.data)
