from rest_framework import serializers
from .models import Loan, LoanComment, LoanFee, LoanCollateral, LoanAttachment, LoanType
from .models import (
    Loan, LoanComment, LoanRepayment,
    LoanCollateral, LoanGuarantor,
    GuarantorFile, LoanDisbursement, LoanScheduler, LoanMembership
)
from staffs.models import Staff

from .models import (Loan, LoanComment, LoanFee, LoanOfficer, OfficerLoan,
LoanCollateral, LoanAttachment, LoanRepayment, GuarantorFile, LoanGuarantor, LoanDisbursement)


class LoanTypeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanType


class LoanCommentSerializer(serializers.ModelSerializer):
    loan = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    borrower = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = LoanComment

    def get_loan(self, obj):
        name = obj.loan.account_no
        return name

    def get_author(self, obj):
        name = obj.author.user_id.user.first_name + " " + str(obj.author.user_id.user.last_name)
        return name

    def get_borrower(self, obj):
        name = obj.loan.borrower.first_name + " " + str(obj.loan.borrower.last_name)
        return name

    def get_date(self, obj):
        if obj.loan.branch.date_format == 'dd/mm/yyyy':
            date_is = obj.date.strftime('%d-%m-%Y')
            return date_is
        elif obj.loan.branch.date_format == 'mm/dd/yyyy':
            date_is = obj.date.strftime('%m-%d-%Y')
            return date_is
        else:
            date_is = obj.date.strftime('%Y-%m-%d')
            return date_is


class LoanCommentSerializer2(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanComment

class LoanOfficerSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    class Meta:
        fields = '__all__'
        model = LoanOfficer

    def get_name(self, obj):
        return obj.staff_id.user_id.user.first_name + ' ' + str(obj.staff_id.user_id.user.last_name)

    def get_branch(self, obj):
        return obj.staff_id.user_id.branch.name

class LoanDisbursementSerializer(serializers.ModelSerializer):
    loan_id = serializers.SerializerMethodField()
    loan_officer_id = serializers.SerializerMethodField()
    class Meta:
        fields = '__all__'
        model = LoanDisbursement

    def get_loan_id(self, obj):
        name = obj.loan.account_no
        return name

    def get_loan_officer_id(self, obj):
        name = obj.loan_officer.user_id.user.first_name + ' ' + str(obj.loan_officer.user_id.user.last_name)
        return name

class LoanMembershipSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanMembership

class LoanCollateralSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = LoanCollateral


class LoanFeeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanFee


class LoanCollateralSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanCollateral

class LoanAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanAttachment

class GuarantorFileSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = GuarantorFile

class GuarantorFileSerializer2(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = GuarantorFile


class LoanGuarantorSerializer(serializers.ModelSerializer):
    guarantor_files = serializers.SerializerMethodField()
    country = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    borrower = serializers.SerializerMethodField()
    date_of_birth = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = LoanGuarantor

    def get_guarantor_files(self, obj):
        guarantor_files = []
        for file in obj.guarantorfile_set.all():
            guarantor_files.append(GuarantorFileSerializer(file).data)
        return guarantor_files

    def get_country(self, obj):
        if obj.country:
            name = obj.country.name
            return name

    def get_branch(self, obj):
        if obj.branch:
            name = obj.branch.name
            return name

    def get_borrower(self, obj):
        if obj.borrower:
            names = obj.borrower.first_name + ' ' + str(obj.borrower.last_name)
            return names

    def get_date_of_birth(self, obj):
        if obj.branch.date_format == 'dd/mm/yyyy':
            date_is = obj.date_of_birth.strftime('%d-%m-%Y')
            return date_is
        elif obj.branch.date_format == 'mm/dd/yyyy':
            date_is = obj.date_of_birth.strftime('%m-%d-%Y')
            return date_is
        else:
            date_is = obj.date_of_birth.strftime('%Y-%m-%d')
            return date_is

class LoanGuarantorSerializer2(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = LoanGuarantor


class LoanRepaymentSerializer(serializers.ModelSerializer):
    loan = serializers.SerializerMethodField()
    collector = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = LoanRepayment

    def get_loan(self, obj):
        name = obj.loan.loan_title
        return name

    def get_collector(self, obj):
        if obj.collector:
            name = obj.collector.staff_id.user_id.user.first_name + ' ' + str(obj.collector.staff_id.user_id.user.last_name)
            return name
        else:
            return False

class LoanRepaymentSerializer2(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = LoanRepayment

class LoanSchedulerSerializer(serializers.ModelSerializer):
    date = serializers.SerializerMethodField()
    loan = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = LoanScheduler

    def get_loan(self, obj):
        name = obj.loan.account_no
        return name

    def get_date(self, obj):
        if obj.loan.branch.date_format == 'dd/mm/yyyy':
            date_is = obj.date.strftime('%d-%m-%Y')
            return date_is
        elif obj.loan.branch.date_format == 'mm/dd/yyyy':
            date_is = obj.date.strftime('%m-%d-%Y')
            return date_is
        else:
            date_is = obj.date.strftime('%Y-%m-%d')
            return date_is

    def get_name(self, obj):
        d_name = obj.loan.borrower.first_name + ' ' + str(obj.loan.borrower.last_name)
        return d_name


class LoanSchedulerSerializer2(serializers.ModelSerializer):
    
    class Meta:
        fields = '__all__'
        model = LoanScheduler


class LoanSerializer(serializers.ModelSerializer):
    borrower = serializers.SerializerMethodField()
    borrower_mobile = serializers.SerializerMethodField()
    branch = serializers.SerializerMethodField()
    loan_type = serializers.SerializerMethodField()
    loan_collateral = serializers.SerializerMethodField()
    loan_guarantor = LoanGuarantorSerializer(many=True)
    loan_fees = LoanFeeSerializer(many=True)
    request_date = serializers.SerializerMethodField()
    loan_release_date = serializers.SerializerMethodField()
    interest_start_date = serializers.SerializerMethodField()
    maturity_date = serializers.SerializerMethodField()
    first_approval_name = serializers.SerializerMethodField()
    second_approval_name = serializers.SerializerMethodField()
    third_approval_name = serializers.SerializerMethodField()
    # remaining_balance = serializers.ReadOnlyField(source="self.get_balance")
    # released = serializers.ReadOnlyField(source="self.released")
    # maturity = serializers.ReadOnlyField(source="self.maturity")
    class Meta:
        fields = '__all__'
        # exclude = ['loan_interest_percentage', 'loan_interest_fixed_amount', 'loan_duration', 'decimal_places', 'charge_in_loan_schedule']
        model = Loan

    def get_borrower(self, obj):
        if obj.borrower:
            borrower = obj.borrower.first_name + ' ' + str(obj.borrower.last_name)
            return borrower

    def get_borrower_mobile(self, obj):
        if obj.borrower:
            borrower = obj.borrower.mobile
            return borrower

    def get_first_approval_name(self, obj):
        if obj.first_approval:
            name = obj.first_approval.user_id.user.first_name + ' ' + obj.first_approval.user_id.user.last_name
            return name

    def get_second_approval_name(self, obj):
        if obj.second_approval:
            name = obj.second_approval.user_id.user.first_name + ' ' + obj.second_approval.user_id.user.last_name
            return name

    def get_third_approval_name(self, obj):
        if obj.third_approval:
            name = obj.third_approval.user_id.user.first_name + ' ' + obj.third_approval.user_id.user.last_name
            return name

    def get_branch(self, obj):
        if obj.branch:
            branch = obj.branch.name
            return branch

    def get_loan_type(self, obj):
        loan_type = obj.loan_type.name
        return loan_type

    def get_loan_collateral(self, obj):
        if obj.loan_collateral:
            loan_collateral = obj.loan_collateral.name
            return loan_collateral

    def get_loan_guarantor(self, obj):
        names = []
        for name in obj.loanguarantor_set.all():
            names.append(LoanGuarantorSerializer(name).data)
        return names

    def get_loan_fees(self, obj):
        names = []
        for name in obj.loan_fees_set.all():
            names.append(LoanFeeSerializer(name).data)
        return names

    def get_request_date(self, obj):
        if obj.branch.date_format == 'dd/mm/yyyy':
            date_is = obj.request_date.strftime('%d-%m-%Y')
            return date_is
        elif obj.branch.date_format == 'mm/dd/yyyy':
            date_is = obj.request_date.strftime('%m-%d-%Y')
            return date_is
        else:
            date_is = obj.request_date.strftime('%Y-%m-%d')
            return date_is

    def get_loan_release_date(self, obj):
        if obj.loan_release_date:
            if obj.branch.date_format == 'dd/mm/yyyy':
                date_is = obj.loan_release_date.strftime('%d-%m-%Y')
                return date_is
            elif obj.branch.date_format == 'mm/dd/yyyy':
                date_is = obj.loan_release_date.strftime('%m-%d-%Y')
                return date_is
            else:
                date_is = obj.loan_release_date.strftime('%Y-%m-%d')
                return date_is

    def get_interest_start_date(self, obj):
        if obj.interest_start_date:
            if obj.branch.date_format == 'dd/mm/yyyy':
                date_is = obj.interest_start_date.strftime('%d-%m-%Y')
                return date_is
            elif obj.branch.date_format == 'mm/dd/yyyy':
                date_is = obj.interest_start_date.strftime('%m-%d-%Y')
                return date_is
            else:
                date_is = obj.interest_start_date.strftime('%Y-%m-%d')
                return date_is

    def get_maturity_date(self, obj):
        if obj.maturity_date:
            if obj.branch.date_format == 'dd/mm/yyyy':
                date_is = obj.maturity_date.strftime('%d-%m-%Y')
                return date_is
            elif obj.branch.date_format == 'mm/dd/yyyy':
                date_is = obj.maturity_date.strftime('%m-%d-%Y')
                return date_is
            else:
                date_is = obj.maturity_date.strftime('%Y-%m-%d')
                return date_is


class LoanSerializer2(serializers.ModelSerializer):
    loan_guarantor = serializers.ListField(write_only=True)
    # loan_guarantor = LoanGuarantorSerializer2()
    # loan_fees = LoanFeeSerializer()
    # loan_fees = serializers.IntegerField(write_only=True)
    # loan_fees = serializers.PrimaryKeyRelatedField(queryset=LoanFee.objects.all(), write_only=True)
    loan_fees = serializers.ListField(write_only=True)

    class Meta:
        model = Loan
        fields = '__all__'

class SummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ['approved']



class OfficerLoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficerLoan
        fields = '__all__'


# class CustomerLoanSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Loan
#         fields = '__all__'
