{% load i18n %}
{% autoescape off %}
{% blocktrans %}

Hello {{ investor_name }},

You have been invited to become an Investor on {{ site_name }}

If you'd like to join, please go to <a href="http://{{ domain }}/?{{ invite_url }}">this link</a>

{% endblocktrans %}
{% endautoescape %}
