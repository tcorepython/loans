from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField
from accounts.models import Profile
from loans.models import LoanOfficer
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
import os
import random
import string
# Create your models here.

def random_ref_generator(size=20, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

ref_gen = random_ref_generator()


class Borrower(models.Model):
    gender_choices = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Prefer not to say', 'Prefer not to say'),
    )
    title_choices = (
        ('Mr', 'Mr'),
        ('Mrs', 'Mrs'),
        ('Miss', 'Miss'),
        ('Dr', 'Dr'),
        ('Ms', 'Ms'),
        ('Rev', 'Rev'),
    )
    working_status_choices = (
        ('Employer', 'Employer'),
        ('Government Employee', 'Government Employee'),
        ('Private Sector', 'Private Sector'),
        ('Employee', 'Employee'),
        ('Owner', 'Owner'),
        ('Student', 'Student'),
        ('Overseas Worker', 'Overseas Worker'),
        ('Pensioner', 'Pensioner'),
        ('Unemployed', 'Unemployed'),
    )
    profile = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    borrower_uid = models.CharField(verbose_name='Borrower Unique ID', max_length=50, blank=True, default='')
    first_name = models.CharField(
        max_length=400)
    middle_name = models.CharField(
        max_length=100, blank=True, null=True, default='')
    last_name = models.CharField(
        max_length=400)
    business_name = models.CharField(
        max_length=400, null=True, blank=True)
    gender = models.CharField(
        choices=gender_choices, max_length=100)
    title = models.CharField(
        choices=title_choices, max_length=100)
    mobile = models.CharField(
        max_length=11)
    email = models.EmailField(max_length=100)
    date_of_birth = models.DateField(
        auto_now_add=False, null=True)
    address = models.CharField(
        max_length=400)
    city = models.CharField(
        max_length=400)
    state = models.CharField(
        max_length=400)
    zip_code = models.CharField(
        max_length=400, blank=True, null=True)
    land_line = models.CharField(
        max_length=400, blank=True, null=True)
    working_status = models.CharField(
        choices=working_status_choices, max_length=100, blank=True)
    borrower_photo = CloudinaryField('image',null=True, blank=True)
    description = models.CharField(
        max_length=400)
    authorization_code = models.CharField(max_length=125, blank=True, null=True)
    is_activated = models.BooleanField(default=False)
    loan_score = models.PositiveIntegerField(blank=True, null=True)
    loan_officer = models.ForeignKey(LoanOfficer, on_delete=models.CASCADE, verbose_name='Loan Officer', default=None, blank=False)
    has_remita = models.BooleanField(default=False)

    def __str__(self):
        return self.first_name + " " + str(self.last_name)

@receiver(pre_save, sender=Borrower)
def update_borrower_uid(sender, instance, **kwargs):
    mobile = instance.mobile
    if len(mobile) > 10:
        m = mobile.startswith("234")
        if m:
            s = mobile[3:]
            instance.borrower_uid = s
        else:
            s = mobile[1:]
            instance.borrower_uid = s
    else:
        instance.borrower_uid = mobile


class Borrower_File(models.Model):
    borrowers_id = models.ForeignKey(Borrower, on_delete=models.SET_NULL, null=True, default='', related_name='borrower_name')
    name = models.CharField('Name', max_length=100, default='')
    pdf_files = models.FileField(upload_to='borrower_files/pdf', default='')
    image_files = models.FileField(upload_to='borrower_files/img', default='')

    def __str__(self):
        return self.name


class BorrowerGroup(models.Model):
    group_name = models.CharField(max_length=255)
    group_leader = models.ForeignKey(Borrower, null=True, blank=True, on_delete=models.DO_NOTHING, related_name='group_leader')
    meeting_date = models.CharField(max_length=255, default='', null=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    members = models.ManyToManyField(Borrower, through='Membership', default=None)
    def __str__(self):
        return self.group_name


class Membership(models.Model):
    borrower = models.ForeignKey(Borrower, on_delete=models.CASCADE)
    borrower_group = models.ForeignKey(BorrowerGroup, on_delete=models.CASCADE)
    date_joined = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.borrower.first_name + " " + "in" + " "+ self.borrower_group.group_name


class InviteBorrower(models.Model):
    email_address = models.CharField(max_length=255, default='', blank=False)
    # borrower = models.ForeignKey(Borrower, on_delete=models.SET_NULL, null=True, default='')

    def __str__(self):
        return self.email_address


class PaystackTransaction(models.Model):
    paystack_charge_id = models.CharField(max_length=100, default=ref_gen, blank=True, null=True, unique=True)
    paystack_access_code = models.CharField(max_length=30, default='', blank=True, null=True)
    customers = models.ForeignKey(Borrower,
                             on_delete=models.SET_NULL, blank=True, null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=20, default=0, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, default='', blank=True, null=True)

    def __str__(self):
        return f"{self.amount} for Card from {self.customers.first_name}  {self.customers.last_name}"

class BorrowerRemita(models.Model):
    borrower = models.ForeignKey(Borrower, on_delete=models.CASCADE, default=None)
    loan = models.ForeignKey(to='loans.Loan', on_delete=models.SET_NULL, null=True)
    payment_date = models.DateField(auto_now_add=False, null=True, blank=True)
    salary_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    account_number = models.CharField(max_length=100, default='', blank=True)
    bank_code = models.CharField(max_length=100, default='', blank=True)
    bank_name = models.CharField(max_length=100, default='', blank=True)

    def __str__(self):
        return self.borrower.first_name + ' ' + self.borrower.last_name
"""
    "loanProvider": null,
    "loanAmount": 2000,
    "outstandingAmount": 2100,
    "loanDisbursementDate": "31-01-2020 11:49:25+0000",
    "status": "NEW",
    "repaymentAmount": 2100,
    "repaymentFreq": "MONTHLY"
"""

class BorrowerLoanRemita(models.Model):
    borrower = models.ForeignKey(Borrower, on_delete=models.CASCADE, default=None)
    loan = models.ForeignKey(to='loans.Loan', on_delete=models.SET_NULL, null=True)
    loan_provider = models.CharField(max_length=100, default='null', null=True, blank=True)
    loan_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    outstanding_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    loan_disbursement_date = models.DateField(auto_now_add=False, null=True, blank=True)
    status = models.CharField(max_length=100, default='', blank=True)
    repayment_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    repayment_frequency = models.CharField(max_length=100, default='', blank=True)

    def __str__(self):
        return self.borrower.first_name + ' ' + self.borrower.last_name
