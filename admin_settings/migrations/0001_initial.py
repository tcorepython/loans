# Generated by Django 3.0.7 on 2020-07-07 15:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LoanPenalty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('enabled_late_repayment_penalty', models.BooleanField(default=False, verbose_name='Enable Late Repayment Penalty?')),
                ('penalty_type', models.CharField(choices=[('Penalty to be percentage % based', 'Penalty to be percentage % based'), ('Penalty to be a fixed amount', 'Penalty to be a fixed amount')], default='Penalty to be percentage % based', max_length=100, verbose_name='Penalty Type')),
                ('calculate_penalty_based_on', models.CharField(choices=[('Overdue Principal Amount', 'Overdue Principal Amount'), ('Overdue Principal + Interest Amount', 'Overdue Principal + Interest Amount'), ('Overdue Principal + Interest + Fees Amount', 'Overdue Principal + Interest + Fees Amount'), ('Overdue Principal + Interest + Fees + Penalty Amount', 'Overdue Principal + Interest + Fees + Penalty Amount')], default='', max_length=150, verbose_name='Calculate Penalty if there is')),
                ('waive_penalty_holidays', models.BooleanField(default=False, verbose_name='Waive penalty on branch holidays?')),
                ('penalty_amount', models.DecimalField(decimal_places=2, default=0, max_digits=100, verbose_name='Penalty Amount')),
                ('grace_period', models.IntegerField(default=0, help_text='For example, if you put 1, then a grace period of 1 day will be given and penalty will be added on 2nd day.', verbose_name='(optional) Grace Period (days)')),
                ('enabled_after_maturity_penalty', models.BooleanField(default=False, verbose_name='Enable After Maturity Date Penalty?')),
                ('mpenalty_type', models.CharField(choices=[('Penalty to be percentage % based', 'Penalty to be percentage % based'), ('Penalty to be a fixed amount', 'Penalty to be a fixed amount')], default='Penalty to be percentage % based', max_length=100, verbose_name='Penalty Type')),
                ('calculate_mpenalty_based_on', models.CharField(choices=[('Overdue Principal Amount', 'Overdue Principal Amount'), ('Overdue Principal + Interest Amount', 'Overdue Principal + Interest Amount'), ('Overdue Principal + Interest + Fees Amount', 'Overdue Principal + Interest + Fees Amount'), ('Overdue Principal + Interest + Fees + Penalty Amount', 'Overdue Principal + Interest + Fees + Penalty Amount')], default='', max_length=150, verbose_name='Calculate Penalty if there is')),
                ('waive_mpenalty_holidays', models.BooleanField(default=False, verbose_name='Maturity Waive penalty on branch holidays?')),
                ('mpenalty_amount', models.DecimalField(decimal_places=2, default=0, max_digits=100, verbose_name='Maturity Penalty Amount')),
                ('mgrace_period', models.IntegerField(default=0, help_text='For example, if you put 1, then a grace period of 1 day will be given and penalty will be added on 2nd day.', verbose_name='(optional) Maturity Grace Period (days)')),
            ],
        ),
        migrations.CreateModel(
            name='LoanSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
    ]
