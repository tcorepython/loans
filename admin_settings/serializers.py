from rest_framework import serializers

from .models import (
	LoanPenalty, LoanSettings
)

class LoanPenaltySerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = LoanPenalty
