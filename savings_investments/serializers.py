from rest_framework import serializers
from .models import *


class SavingsAccountSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()
    profile = serializers.SerializerMethodField()
    savings_product = serializers.SerializerMethodField()
    
    class Meta:
        model = SavingsAccount
        fields = '__all__'

    def get_branch(self, obj):
        branch = obj.branch.name
        return branch

    def get_profile(self, obj):
        profile = obj.profile.user.first_name + ' ' + str(obj.profile.user.last_name)
        return profile

    def get_savings_product(self, obj):
        pro = obj.savings_product.name
        return pro

class SavingsAccountSerializer2(serializers.ModelSerializer):
    class Meta:
        model = SavingsAccount
        fields = '__all__'


class SavingsProductSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()
    class Meta:
        model = SavingsProduct
        # fields = '__all__'
        exclude = ['deposit', 'transfer_in', 'withdrawal', 'fees',
                   'interest', 'dividend', 'transfer_out', 'commission',
                   'balance']

    def get_branch(self, obj):
        name = obj.branch.name
        return name


class SavingsProductSerializer2(serializers.ModelSerializer):
    class Meta:
        model = SavingsProduct
        fields = '__all__'


class SavingsProductReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavingsProduct
        fields = ['deposit', 'transfer_in', 'withdrawal', 'fees',
                  'interest', 'dividend', 'transfer_out', 'commission',
                  'balance']


class CashSourceSerializer(serializers.ModelSerializer):
    cash_safe_management = serializers.SerializerMethodField()
    
    class Meta:
        model = CashSource
        fields = '__all__'

    def get_cash_safe_management(self, obj):
        name = obj.cash_safe_management.branch.name
        return name


class CashSourceSerializer2(serializers.ModelSerializer):
    class Meta:
        model = CashSource
        fields = '__all__'


class TellerSerializer(serializers.ModelSerializer):
    staff = serializers.SerializerMethodField()
    cash_safe_management = serializers.SerializerMethodField()
    class Meta:
        model = Teller
        exclude = ['report_deposit', 'report_transfer_in', 'report_withdrawal', 'report_fees',
                   'report_interest', 'report_dividend', 'report_transfer_out', 'report_commission',
                   'report_balance']

    def get_staff(self, obj):
        name = obj.staff.user.first_name + ' ' + str(obj.staff.user.last_name)
        return name

    def get_cash_safe_management(self, obj):
        name = obj.cash_safe_management.branch.name
        return name


class TellerReportSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Teller
        fields = ['name', 'report_deposit', 'report_transfer_in', 'report_withdrawal', 'report_fees',
                  'report_interest', 'report_dividend', 'report_transfer_out', 'report_commission',
                  'report_balance']

    def get_name(self, obj):
        return obj.staff.user.username


class TransferCashSerializer(serializers.ModelSerializer):
    cash_safe_management = serializers.SerializerMethodField()
    from_cash_source = serializers.SerializerMethodField()
    to_cash_source = serializers.SerializerMethodField()
    to_teller = serializers.SerializerMethodField()
    from_teller = serializers.SerializerMethodField()

    class Meta:
        model = TransferCash
        fields = '__all__'

    def get_cash_safe_management(self, obj):
        name = obj.cash_safe_management.branch.name
        return name

    def get_from_cash_source(self, obj):
        name = obj.from_cash_source.name
        return name

    def get_to_cash_source(self, obj):
        name = obj.to_cash_source.name
        return name

    def get_to_teller(self, obj):
        if obj.to_teller:
            name = obj.to_teller.teller_no
            return name

    def get_from_teller(self, obj):
        if obj.to_teller:
            name = obj.from_teller.teller_no
            return name


    # def raise_error(self):
    #     raise ValidationError("Incorrect request, transfer should"
    #                           " occur between 'from_cash_source' - 'to_cash_source', "
    #                           "'from_cash_source' - 'to_teller', 'from_teller' - 'to_teller', "
    #                           "or 'from_teller' - 'to_cash_source'")

    # def validate(self, attrs):
    #     # validate transfer cash
    #     transfer_counter = 0
    #     if attrs.get("from_cash_source"):
    #         transfer_counter += 1
    #     if attrs.get("to_cash_source"):
    #         transfer_counter += 1
    #     if attrs.get("from_teller"):
    #         transfer_counter += 1
    #     if attrs.get("to_teller"):
    #         transfer_counter += 1
    #     if transfer_counter != 2:
    #         self.raise_error()
    #     if (attrs.get("from_cash_source") and attrs.get("to_cash_source")) or \
    #             (attrs.get("from_cash_source") and attrs.get("to_teller")) or \
    #             (attrs.get("from_teller") and attrs.get("to_teller")) or \
    #             (attrs.get("from_teller") and attrs.get("to_cash_source")):

    #         if (attrs.get('to_cash_source') is not None) and (attrs.get('from_cash_source') is not None):
    #             if attrs.get("to_cash_source") == attrs.get("from_cash_source"):
    #                 raise ValidationError("impossible to transfer cash from same cash source")

    #         if (attrs.get('to_teller') is not None) and (attrs.get('from_teller') is not None):
    #             if attrs.get("to_teller") == attrs.get("from_teller"):
    #                 raise ValidationError("impossible to transfer cash from same teller")
    #         return attrs
    #     self.raise_error()


class TransferCashSerializer2(serializers.ModelSerializer):
    class Meta:
        model = TransferCash
        fields = '__all__'

    def raise_error(self):
        raise ValidationError("Incorrect request, transfer should"
                              " occur between 'from_cash_source' - 'to_cash_source', "
                              "'from_cash_source' - 'to_teller', 'from_teller' - 'to_teller', "
                              "or 'from_teller' - 'to_cash_source'")

    def validate(self, attrs):
        # validate transfer cash
        transfer_counter = 0
        if attrs.get("from_cash_source"):
            transfer_counter += 1
        if attrs.get("to_cash_source"):
            transfer_counter += 1
        if attrs.get("from_teller"):
            transfer_counter += 1
        if attrs.get("to_teller"):
            transfer_counter += 1
        if transfer_counter != 2:
            self.raise_error()
        if (attrs.get("from_cash_source") and attrs.get("to_cash_source")) or \
                (attrs.get("from_cash_source") and attrs.get("to_teller")) or \
                (attrs.get("from_teller") and attrs.get("to_teller")) or \
                (attrs.get("from_teller") and attrs.get("to_cash_source")):

            if (attrs.get('to_cash_source') is not None) and (attrs.get('from_cash_source') is not None):
                if attrs.get("to_cash_source") == attrs.get("from_cash_source"):
                    raise ValidationError("impossible to transfer cash from same cash source")

            if (attrs.get('to_teller') is not None) and (attrs.get('from_teller') is not None):
                if attrs.get("to_teller") == attrs.get("from_teller"):
                    raise ValidationError("impossible to transfer cash from same teller")
            return attrs
        self.raise_error()


class SavingsTransactionSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()
    teller = serializers.SerializerMethodField()
    savings_account = serializers.SerializerMethodField()
    date_time = serializers.SerializerMethodField()

    class Meta:
        model = SavingsTransaction
        fields = '__all__'

    def get_branch(self, obj):
        name = obj.branch.name
        return name

    def get_teller(self, obj):
        if obj.teller:
            name = obj.teller.cash_safe_management.branch.name
            return name

    def get_savings_account(self, obj):
        name = obj.savings_account.savings_id
        return name

    def get_date_time(self, obj):
        if obj.branch.date_format == 'dd/mm/yyyy':
            date_is = obj.date_time.strftime('%d-%m-%Y, %I:%M:%p')
            return date_is
        elif obj.branch.date_format == 'mm/dd/yyyy':
            date_is = obj.date_time.strftime('%m-%d-%Y, %I:%M:%p')
            return date_is
        else:
            date_is = obj.date_time.strftime('%Y-%m-%d, %I:%M:%p')
            return date_is


class SavingsTransactionSerializer2(serializers.ModelSerializer):
    class Meta:
        model = SavingsTransaction
        fields = '__all__'


class TransferFundSerializer2(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = FundTransferLog

    def validate(self, attrs):
        if attrs['from_account'] == attrs['to_account']:
            raise serializers.ValidationError("cannot transfer fund from/to same account")
        return attrs


class TransferFundSerializer(serializers.ModelSerializer):
    from_account = serializers.SerializerMethodField()
    to_account = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = FundTransferLog

    def get_from_account(self, obj):
        name = obj.from_account.savings_id
        return name

    def get_to_account(self, obj):
        name = obj.to_account.savings_id
        return name


class SavingsFeeSerializer2(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = SavingsFee


class SavingsFeeSerializer(serializers.ModelSerializer):
    savings_product = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = SavingsFee

    def get_savings_product(self, obj):
        name = obj.savings_product.name
        return name
