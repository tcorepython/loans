from django.shortcuts import render
from .serializers import LoanPenaltySerializer
import calendar
import json
import requests
import hashlib
import datetime
import random
import decimal
from datetime import timedelta
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from dateutil.relativedelta import relativedelta
from rest_framework.views import APIView
from rest_framework import status, viewsets, permissions
from loan_management_system import settings
import request
from rest_framework import status
from rest_framework.response import Response
from django.utils.dateparse import parse_date


class LoanPenaltyViewSet(viewsets.ModelViewSet):
	queryset = LoanPenalty.objects.all()
	serializer_classes = LoanPenaltySerializer