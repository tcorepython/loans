from rest_framework.permissions import BasePermission
from rest_framework import permissions


class IsSuperAdmin(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        return user.profile.is_super_admin == True

    def has_object_permission(self, request, view, obj):
        user = request.user
        return user.profile.is_super_admin == True


class IsSuperAdminOrReadOnly(BasePermission):

    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        return request.user.profile.is_super_admin == True

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        return request.user.profile.is_super_admin == True


class IsStaffOrAdmin(BasePermission):

    def has_permission(self, request, view):
        if view.action == 'GET':
            return False
        elif view.action == 'POST':
            return request.user.profile.user_type in ['admin', 'staff']
        elif view.action in ['GET', 'PATCH', 'DELETE']:
            return request.user.profile.user_type in ['admin', 'staff']
        else:
            return False

    def has_object_permission(self, request, view, obj):
        # Deny actions on objects if the user is not authenticated
        if not request.user:
            return False

        if view.action == 'GET':
            return obj == True
        elif view.action in ['PATCH']:
            return obj == request.user.profile.user_type in ['admin', 'staff']
        elif view.action == 'DELETE':
            return request.user.profile.user_type in ['admin', 'staff']
        else:
            return False
    # def has_permission(self, request, view):
    #     user = request.user
    #     return user.profile.user_type in ['admin', 'staff']

    # def has_object_permission(self, request, view, obj):
    #     user = request.user
    #     return user.profile.user_type in ['admin', 'staff']


class IsStaffOrAdminOrReadOnly(BasePermission):

    def has_permission(self, request, view):
        if request.method == 'GET':
            # return True
            return request.user.profile.user_type in ['admin', 'staff']

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            # return True
            return request.user.profile.user_type in ['admin', 'staff']


class IsOwner(BasePermission):

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        print(obj)
        return True


class IsOwnerOrStaffOrAdmin(BasePermission):

    def has_permission(self, request, view):
        return request.method in ['GET', 'PUT', 'PATCH']

    def has_object_permission(self, request, view, obj):
        return (request.user == obj) or (request.user.profile.user_type in ['staff', 'admin'])
