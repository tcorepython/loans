from django.db import models

# Create your models here.
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import base64
import uuid
from decimal import Decimal
import decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.db.models import Sum
from loans.models import LoanType

from django.db.models.signals import post_save
from django.dispatch import receiver
import random
import string


class LoanSettings(models.Model):

    interest_type_types = (
        ('Percentage Based', 'Percentage Based'),
        ('Fixed Amount Per Cycle', 'Fixed Amount Per Cycle'),
    )
    interest_method_types = (
        ('Flat Rate', 'Flat Rate'),
        ('Reducing Balance - Equal Installments','Reducing Balance - Equal Installments'),
        ('Reducing Balance - Equal Principal','Reducing Balance - Equal Principal'),
        ('Interest-Only', ' Interest-Only'),
        ('Compound-Interest', 'Compound Interest'),
    )
    # loan_interest_percentage = models.DecimalField(verbose_name=_('Loan Interest Percentage'),
				# 				max_digits=100, default=0, decimal_places=2)
    # loan_interest_fixed_amount = models.DecimalField(verbose_name=_('Loan Interest Fixed Amount'),
				# 				max_digits=100, default='', decimal_places=2)
    # loan_interest_percentage_period = models.CharField(verbose_name=_('Loan Interest Percentage Period'),
				# 				max_length=100, default='', blank=False)
    # interest_method = models.CharField(verbose_name=_('Interest Method'),
    #     						choices=interest_method_types, max_length=100)
    # interest_mode = models.CharField(verbose_name=_('Interest Mode'),
    #     						choices=interest_type_types, max_length=400, blank=True, null=True, 
    #     						default='Percentage Based')
    # transfer_fee = models.DecimalField(verbose_name=_('Transfer Fee'), max_digits=100,
    # 							default=0, decimal_places=2)
    # loan_fees = models.DecimalField(verbose_name=_('Loan Fee'), max_digits=100, decimal_places=2,
    # 							default=0)
    # penalty_amount = models.DecimalField(verbose_name=_('Penalty Amount'), max_digits=100,
    # 							decimal_places=2, default=0.0)
    # interest = models.DecimalField(verbose_name=_('Interest'), max_digits=100, decimal_places=2,
    # 							null=True, blank=True, default=0)
    # total_due_principal = models.DecimalField(verbose_name=_('Total Due Principal'), max_digits=100, decimal_places=2, null=True, blank=True, default=0)
    # total_due_interest = models.DecimalField(verbose_name=_('Total Due Interest'), max_digits=100, decimal_places=2, null=True, blank=True, default=0)
    # total_due_loan_fee = models.DecimalField(verbose_name=_('Total Due Loan Fee'), max_digits=100, decimal_places=2, null=True, blank=True, default=0)
    # total_due_penalty = models.DecimalField(verbose_name=_('Total Due Penalty'), max_digits=100, decimal_places=2, null=True, blank=True, default=0)


class LoanPenalty(models.Model):
    loan_type_l = models.OneToOneField(LoanType, on_delete=models.CASCADE, default=None)
    late_repayment_penalty_choice = (
        ('Penalty to be percentage % based', 'Penalty to be percentage % based'),
        ('Penalty to be a fixed amount', 'Penalty to be a fixed amount')
    )
    calculate_penalty_based_choices = (
        ('Overdue Principal Amount', 'Overdue Principal Amount'),
        ('Overdue Principal + Interest Amount', 'Overdue Principal + Interest Amount'),
        ('Overdue Principal + Interest + Fees Amount', 'Overdue Principal + Interest + Fees Amount'),
        ('Overdue Principal + Interest + Fees + Penalty Amount', 'Overdue Principal + Interest + Fees + Penalty Amount'),
    )
    enabled_late_repayment_penalty = models.BooleanField(default=False,
                            verbose_name=_('Enable Late Repayment Penalty?'))
    penalty_type = models.CharField('Penalty Type', max_length=100, choices=late_repayment_penalty_choice,
                                    default='Penalty to be percentage % based')
    calculate_penalty_based_on = models.CharField(verbose_name=_('Calculate Penalty if there is'),
                            max_length=150, choices=calculate_penalty_based_choices, default='')
    waive_penalty_holidays = models.BooleanField(default=False,
                            verbose_name=_('Waive penalty on branch holidays?'))
    penalty_amount = models.DecimalField(verbose_name=_('Penalty Amount'),
                            max_digits=100, decimal_places=2, default=0)
    grace_period = models.IntegerField(verbose_name=_('(optional) Grace Period (days)'),
                            help_text=_('For example, if you put 1, then a grace period of 1 day will be given and penalty will be added on 2nd day.'),
                            default=0)
    #### Maturity Date Penalty
    enabled_after_maturity_penalty = models.BooleanField(default=False,
                            verbose_name=_('Enable After Maturity Date Penalty?'))
    mpenalty_type = models.CharField('Penalty Type', max_length=100, choices=late_repayment_penalty_choice,
                                    default='Penalty to be percentage % based')
    calculate_mpenalty_based_on = models.CharField(verbose_name=_('Calculate Penalty if there is'),
                            max_length=150, choices=calculate_penalty_based_choices, default='')
    waive_mpenalty_holidays = models.BooleanField(default=False,
                            verbose_name=_('Maturity Waive penalty on branch holidays?'))
    mpenalty_amount = models.DecimalField(verbose_name=_('Maturity Penalty Amount'),
                            max_digits=100, decimal_places=2, default=0)
    mgrace_period = models.IntegerField(verbose_name=_('(optional) Maturity Grace Period (days)'),
                            help_text=_('For example, if you put 1, then a grace period of 1 day will be given and penalty will be added on 2nd day.'),
                            default=0)
