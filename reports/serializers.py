from rest_framework import serializers
from .models import (
    CalendarEvent, CalendarEventEmail, CalendarLog,
    OtherIncomeType, OtherIncome, OtherIncomeDocuments, LoanBorrowerReport, LoanReport
)
from loans.models import Loan, OfficerLoan, LoanOfficer


class CalendarLogSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = CalendarLog


class CalendarEventSerializer(serializers.ModelSerializer):
    emails = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = CalendarEvent

    def get_emails(self, obj):
        emails = []
        for email in obj.calendareventemail_set.all():
            emails.append(CalendarEventEmailSerializer(email).data)
        return emails


class CalendarEventEmailSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = CalendarEventEmail


class OtherIncomeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = OtherIncomeType


class OtherIncomeSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = OtherIncome


class OtherIncomeDocumentsSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = OtherIncomeDocuments



class LoanBorrowerReportSerializer(serializers.ModelSerializer):
    # borrower = serializers.SerializerMethodField()
    
    class Meta:
        model = LoanBorrowerReport
        fields = '__all__'

    # def get_borrower(self, obj):
    #     name = obj.borrower.id
    #     return name
    # def get_date(self, obj):
    #     if obj.borrower.profile.profile.branch.date_format == 'dd/mm/yyyy':
    #         date_is = obj.date.strftime('%d-%m-%Y')
    #         return date_is
    #     elif obj.borrower.profile.profile.branch.date_format == 'mm/dd/yyyy':
    #         date_is = obj.date.strftime('%m-%d-%Y')
    #         return date_is
    #     else:
    #         date_is = obj.date.strftime('%Y-%m-%d')
    #         return date_is


class LoanBorrowerReportSerializer2(serializers.ModelSerializer):
    borrower = serializers.SerializerMethodField()
    class Meta:
        model = LoanBorrowerReport
        fields = '__all__'

    def get_borrower(self, obj):
        name = obj.borrower.first_name
        return name


class LoanReportSerializer(serializers.ModelSerializer):
    # borrower = serializers.SerializerMethodField()
    class Meta:
        model = LoanReport
        fields = '__all__'

