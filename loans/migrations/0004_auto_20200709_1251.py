# Generated by Django 3.0.7 on 2020-07-09 11:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0003_auto_20200709_1250'),
    ]

    operations = [
        migrations.RenameField(
            model_name='loanrepayment',
            old_name='next_due',
            new_name='next_due_date',
        ),
        migrations.RenameField(
            model_name='loanrepayment',
            old_name='past_due',
            new_name='past_due_date',
        ),
    ]
