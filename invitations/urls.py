from django.conf.urls import url
from django.conf.urls import include, re_path
from rest_framework.routers import DefaultRouter

from .views import SendInviteViewSet, SendInvestorInviteViewSet

router = DefaultRouter()

app_name = 'invitations'
# urlpatterns = [
#     url(r'^send-invite/$', views.SendInviteViewSet,
#         name='send-invite'),

#     url(r'^send-json-invite/$', views.SendJSONInvite.as_view(),
#         name='send-json-invite'),

#     url(r'^accept-invite/(?P<key>\w+)/?$', views.AcceptInvite.as_view(),
#         name='accept-invite'),
# ]

router.register('borrowers', SendInviteViewSet, basename='send-borrower-invite')
router.register('investors', SendInvestorInviteViewSet, basename='send-investor-invite')
# router.register('accept-invite', AcceptInviteViewSet, basename='accept-invite')

urlpatterns = [
    re_path('^', include(router.urls)),
]