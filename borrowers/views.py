from django.shortcuts import render
from django.db.models import Q
from loan_management_system import settings

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, JSONParser, FileUploadParser
from rest_framework import status

from .models import *
import request
from loans.models import Loan, LoanRepayment, LoanDisbursement
from loans.serializers import LoanSerializer, LoanRepaymentSerializer
from .serializers import *
from savings_investments.models import SavingsAccount
from savings_investments.serializers import SavingsAccountSerializer
import cloudinary.uploader
# from loan_management_system import permissions as perms
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import viewsets


@api_view(['GET', 'DELETE', 'PATCH', 'POST'])
def get_delete_update_borrower(request, pk):
    try:
        borrower = Borrower.objects.get(pk=pk)
    except Borrower.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        # permission_classes = (perms.IsStaffOrAdmin,)
        serializer = BorrowerSerializer(borrower)
        return Response(serializer.data)

    elif request.method == 'PATCH':
        # permission_classes = (perms.IsStaffOrAdmin,)
        serializer = BorrowerSerializer2(borrower, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    elif request.method == 'DELETE':
        borrower.delete()
        return Response("Borrower deleted successfully", status=204)


@api_view(['GET', 'POST'])
def get_post_borrower(request):
    # get all restaurants
    if request.method == 'GET':
        # permission_classes = (perms.IsStaffOrAdmin,)
        ref = request.GET.get("borrower_search")
        if ref:
            borrower = Borrower.objects.filter(Q(first_name__startswith=ref) | Q(middle_name__startswith=ref) | Q(last_name__startswith=ref))
            serializer = BorrowerSerializer(borrower, many=True)
            return Response(serializer.data)
        borrowers = Borrower.objects.all()
        serializer = BorrowerSerializer(borrowers, many=True)
        return Response(serializer.data)
    # insert a new record for a restaurant
    elif request.method == 'POST':
        data = {
            'profile': request.data.get('profile'),
            'first_name': request.data.get('first_name'),
            'middle_name': request.data.get('middle_name'),
            'last_name': request.data.get('last_name'),
            'business_name': request.data.get('business_name'),
            'gender': request.data.get('gender'),
            'title': request.data.get('title'),
            'mobile': request.data.get('mobile'),
            'email': request.data.get('email'),
            'date_of_birth': request.data.get('date_of_birth'),
            'address': request.data.get('address'),
            'city': request.data.get('city'),
            'state': request.data.get('state'),
            'zip_code': request.data.get('zip_code'),
            'land_line': request.data.get('land_line'),
            'working_status': request.data.get('working_status'),
            'borrower_photo': request.data.get('borrower_photo'),
            'description': request.data.get('description'),
            'is_activated': request.data.get('is_activated'),
            'borrower_group': request.data.get('borrower_group'),
            'loan_officer': request.data.get('loan_officer'),
        }
        if data['borrower_photo'] != '':
            upload_data = cloudinary.uploader.upload(data['borrower_photo'])
            data['borrower_photo'] = upload_data['url']
        print(data['loan_officer'])
        serializer = BorrowerSerializer2(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BorrowerFileList(APIView):
    parser_class = (FileUploadParser,)
    def post(self, request):
        serializer = BorrowerFileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, pk=None):
        queryset = Borrower_File.objects.all()
        borrower = self.request.GET.get('borrower')
        # if borrower:
        #     return LoanCollateral.objects.filter(branch=branch)
        borrower_files = Borrower_File.objects.filter(borrowers_id=borrower)
        print(borrower_files)
        serializer = BorrowerFileSerializer(borrower_files, many=True)
        return Response(serializer.data)

class BorrowerFileDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return Borrower_File.objects.get(pk=pk)
        except Borrower_File.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        borrower_files = self.get_object(pk)
        serializer = BorrowerFileSerializer(borrower_files)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        borrower_files = self.get_object(pk)
        serializer = BorrowerFileSerializer(borrower_files, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        borrower_files = self.get_object(pk)
        borrower_files.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'DELETE', 'PATCH'])
def get_delete_update_borrower_group(request, pk):
    try:
        borrower_group = BorrowerGroup.objects.get(pk=pk)
    except BorrowerGroup.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = BorrowerGroupSerializer(borrower_group)
        return Response(serializer.data)

    elif request.method == 'PATCH':
        serializer = BorrowerGroupSerializer(borrower_group, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    elif request.method == 'DELETE':
        borrower_group.delete()
        return Response("Borrower group deleted successfully", status=204)


@api_view(['GET', 'POST'])
def get_post_borrower_group(request):
    # get all restaurants
    if request.method == 'GET':
        borrower_groups = BorrowerGroup.objects.all()
        serializer = BorrowerGroupSerializer(borrower_groups, read_only=True, many=True)
        return Response(serializer.data)
    # insert a new record for a restaurant
    elif request.method == 'POST':
        data = {
            'group_name': request.data.get('group_name'),
            'group_leader': request.data.get('group_leader'),
            'meeting_date': request.data.get('meeting_date'),
            'description': request.data.get('description'),
        }
        serializer = BorrowerGroupSerializer2(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def add_to_group(request):
    # get all restaurants
    if request.method == 'GET':
        membership = Membership.objects.all()
        serializer = MembershipSerializer(membership, many=True)
        return Response(serializer.data)
    # insert a new record for a restaurant
    elif request.method == 'POST':
        data = {
            'borrower': request.data.get('borrower'),
            'borrower_group': request.data.get('borrower_group'),

        }
        serializer = MembershipSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def SearchBorrowerGroup(request):
    # get all restaurants
    ref = request.GET.get("ref")
    if request.method == 'GET':
        borrower_groups = BorrowerGroup.objects.filter(Q(group_name__startswith=ref))
        serializer = BorrowerGroupSerializer(borrower_groups, many=True)
        return Response(serializer.data)
    # insert a new record for a restaurant

@api_view(['GET'])
def IndividualOpenLoans(request):
    id = request.GET.get("id")
    # get all restaurants
    if request.method == 'GET':
        members_loan = []
        borrower_group = BorrowerGroup.objects.get(pk=int(id))
        group_members = borrower_group.members.all()
        for g in group_members:
            member_loan = []
            member_loan = Loan.objects.filter(borrower=g.pk).exclude(status="denied").exclude(status="processing").exclude(status="fully paid")
            if(len(member_loan)>0):
                for unit in member_loan:               
                    members_loan.append(unit)  
            else:
                return Response("No response")  
        serializer = LoanSerializer(members_loan, many=True)
        return Response({unit.borrower.id: serializer.data})

@api_view(['GET'])
def IndividualRepayments(request):
    id = request.GET.get("id")
    # get all restaurants
    if request.method == 'GET':
        total = []
        members_loan = []
        result = []
        borrower_group = BorrowerGroup.objects.get(pk=int(id))
        group_members = borrower_group.members.all()
        for g in group_members:
            member_loan = []
            repayments = []
            member_loan = Loan.objects.filter(borrower=g.pk).exclude(status="denied").exclude(status="processing").exclude(status="fully paid")
            for each_loan in member_loan:
                loan_rts = []
                loan_repayments = LoanRepayment.objects.filter(loan=each_loan)
                if len(loan_repayments) > 0:
                    unit_r = []
                    for each_loan_repayment in loan_repayments:
                        unit_r.append(each_loan_repayment)
                    loan_rts.append({each_loan_repayment.pk: LoanRepaymentSerializer(unit_r, many=True).data})
                total.append(({g.id: loan_rts}))

            # if(len(member_loan)>0):
            #     for unit in member_loan:               
        #             members_loan.append(unit)  
        #     else:
        #         return Response("No response")  
        # serializer = LoanSerializer(members_loan, many=True)
        return Response(total)


@api_view(['GET'])
def BorrowersSavings(request):
    id = request.GET.get("id")
    if request.method == 'GET':
        saving_account = []
        borrower = Borrower.objects.filter(pk=int(id))[0]
        print(Borrower.objects.filter(pk=int(id)))
        print(Borrower.objects.filter(pk=int(id))[0])
        savings_account = SavingsAccount.objects.filter(profile=borrower.profile.pk)
        serializer = SavingsAccountSerializer(savings_account, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def SearchByWorkingStatus(request, status=None):
    #id = request.GET.get("id")
    # get all restaurants
    if request.method == 'GET':
        #members_loan = []
        borrower = Borrower.objects.filter(working_status = status.capitalize())
        # for unit in borrower_group:
        #     print(unit.member)
        #     members_loan.append(unit.member)
        #print(members_loan)
        # members = borrower_group.member
        # print(members)
        serializer = BorrowerSerializer(borrower, many=True)
        return Response(serializer.data)



@api_view(['GET'])
def MembersOfGroupLoans(request, pk=None):
    if request.method == 'GET':
        borrower = Borrower.objects.filter(pk=pk)
        serializer = SavingsAccountSerializer(savings_account, many=True)
        return Response(serializer.data)


@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_invite_borrower(request, pk):
    try:
        invite_borrower = InviteBorrower.objects.get(pk=pk)
    except InviteBorrower.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        serializer = InviteBorrowerSerializer(invite_borrower)
        return Response(serializer.data)
    elif request.method == 'PATCH':
        serializer = InviteBorrowerSerializer(invite_borrower, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    elif request.method == 'DELETE':
        invite_borrower.delete()
        return Response("Prospective Borrower deleted successfully", status=204)


@api_view(['GET', 'POST'])
def get_post_invite_borrower(request):
    # get all restaurants
    if request.method == 'GET':
        invite_borrower = InviteBorrower.objects.all()
        serializer = InviteBorrowerSerializer(invite_borrower, many=True)
        return Response(serializer.data)
    # insert a new record for a restaurant
    elif request.method == 'POST':
        data = {
            'email_address': request.data.get('email_address'),
        }
        serializer = InviteBorrowerSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PaystackViewSet(viewsets.ModelViewSet):
    queryset = PaystackTransaction.objects.all()
    lookup_field = 'paystack_charge_id'
    serializer_class = PaystackSerializer

    def create(self, request, *args, **kwargs):
        serializer = PaystackSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        obj = serializer.save()

        if obj:
            amount = obj.amount
            reference = obj.paystack_charge_id
            amount = str(amount)
            # amount = amount.replace('.00', '')
            chk_user = Borrower.objects.filter(id=obj.customers.id).exists()
            print(chk_user)
            if chk_user:
                get_usr_id = Borrower.objects.get(id=obj.customers.id)
                d_email = get_usr_id.email
            def make_transaction(request):
                url = 'https://api.paystack.co/transaction/initialize'
                headers = {
                    'Authorization': 'Bearer '+settings.PAYSTACK_SECRET,
                    'Content-Type' : 'application/json',
                }
                payload = {
                    'reference': reference,
                    'amount': amount,
                    'email': d_email
                }
                x = requests.post(url, headers=headers, data=json.dumps(payload))
                if x.status_code != 200:
                    return str(x.status_code)
      
                results = x.json()
                return results
            make_trans = make_transaction(request)
            get_ref = make_trans
            PaystackTransaction.objects.filter(id=obj.id).update(paystack_access_code=get_ref['data']['access_code'], status=get_ref['status'])

        return Response({"data": get_ref, "message": serializer.data}, status=status.HTTP_200_OK)


class VerifyTransaction(APIView):

    def get(self, request, paystack_charge_id=None):
        paystack_charge_id = request.GET.get("paystack_charge_id")
        the_pay_id = PaystackTransaction.objects.get(paystack_charge_id=paystack_charge_id)
        def verify_transaction(request):
                url = 'https://api.paystack.co/transaction/verify/'+str(paystack_charge_id)
                headers = {
                    'Authorization': 'Bearer '+settings.PAYSTACK_SECRET,
                    'Content-Type' : 'application/json',
                }
                x = requests.get(url, headers=headers)
                if x.status_code != 200:
                    return str(x.status_code)
      
                results = x.json()
                return results
        rez = verify_transaction(paystack_charge_id)
        # print(rez['data']['authorization']['authorization_code'])
        if rez == False:
            return Response({"message": "Error Reference Code."}, status=status.HTTP_400_BAD_REQUEST)
        else:
            if the_pay_id.customers:
                Borrower.objects.filter(id=the_pay_id.customers.id).update(authorization_code=rez['data']['authorization']['authorization_code'])
            return Response({"message": "Payment Verified Successfully", "data from paystack": rez},
                            status=status.HTTP_200_OK)


class RemitaSalary(APIView):
    def get(self, request):
        loan_id = request.GET.get('loan')
        borrower_mobile = request.GET.get('borrower_mobile')
        check_loan = Loan.objects.filter(id=loan_id).exists()
        if check_loan == False:
            return Response({'not_found': 'No loan with such ID'}, status=status.HTTP_404_NOT_FOUND)
        loan = Loan.objects.get(id=loan_id)
        check_remita = BorrowerRemita.objects.filter(loan=loan, borrower=loan.borrower).exists()
        if check_remita == False:
            # if not borrower_mobile:
            #     borrower_mobile = loan.borrower.mobile
            url = 'https://api.touchcore.tech/v1/remita/get-payment-history'
            headers = {
                "Authorization": "Bearer sk_f603f240ba3602994c03a3604670a01c17b857c9",
                "Content-Type": "application/json",
                'HOST': "api.touchcore.tech",
                'User-Agent': "RMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
                'Accept': "*/*"
            }
            datum = {
                # "phone_number": "08154567478",
                "phone_number": borrower_mobile,
                "authorisation_channel": "USSD"
            }
            x = requests.get(url, headers=headers, data=datum, params=datum)
            if x.status_code !=200 and not 422:
                return Response({'error': 'Error. Please try again.'}, status=status.HTTP_400_BAD_REQUEST)
            elif x.status_code==422:
                results = x.json()
                return Response(results, status=status.HTTP_404_NOT_FOUND)
            else:
                results = x.json()
                Borrower.objects.filter(id=loan.borrower.id).update(mobile=borrower_mobile, authorization_code=results['data']['authorization_code'])
                Loan.objects.filter(id=loan.id).update(has_remita=True, customer_remita_id=results['data']['customerId'])
                data = []
                data1 = []
                for infos in results['data']['loanHistoryDetails']:
                    if check_remita == False:
                        if loan.branch.date_format == 'dd/mm/yyyy':
                            date_dis = datetime.datetime.strptime(infos['loanDisbursementDate'], '%d-%m-%Y %H:%M:%S.%f')
                            dis_date = date_dis
                        elif loan.branch.date_format == 'mm/dd/yyyy':
                            date_dis = datetime.datetime.strptime(infos['loanDisbursementDate'], '%m-%d-%Y %H:%M:%S.%f')
                            dis_date = date_dis
                        else:
                            date_dis = datetime.datetime.strptime(infos['loanDisbursementDate'], '%d-%m-%Y %H:%M:%S+%f')
                            dis_date = date_dis
                        BorrowerLoanRemita.objects.create(
                            loan_provider = infos['loanProvider'],
                            loan_amount = infos['loanAmount'],
                            loan_disbursement_date= dis_date.date(),
                            outstanding_amount = infos['outstandingAmount'],
                            status = infos['status'],
                            repayment_amount = infos['repaymentAmount'],
                            repayment_frequency = infos['repaymentFreq'],
                            borrower = loan.borrower,
                            loan = loan
                        )

                for info in results['data']['salaryPaymentDetails']:
                    if check_remita == False:
                        if loan.branch.date_format == 'dd/mm/yyyy':
                            date_is = datetime.datetime.strptime(info['paymentDate'], '%d-%m-%Y %H:%M:%S.%f')
                            d_date = date_is
                        elif loan.branch.date_format == 'mm/dd/yyyy':
                            date_is = datetime.datetime.strptime(info['paymentDate'], '%m-%d-%Y %H:%M:%S.%f')
                            d_date = date_is
                        else:
                            date_is = datetime.datetime.strptime(info['paymentDate'], '%d-%m-%Y %H:%M:%S+%f')
                            d_date = date_is
                        BorrowerRemita.objects.create(
                            salary_amount = info['amount'],
                            account_number = info['accountNumber'],
                            payment_date= d_date.date(),
                            bank_code = info['bankCode'],
                            bank_name = results['data']['bankName'],
                            borrower = loan.borrower,
                            loan = loan
                        )
                    datas = {
                        'salary_amount': info['amount'],
                        'account_number': info['accountNumber'],
                        'payment_date': info['paymentDate'],
                        'bank_code': info['bankCode'],
                        'bank_name': results['data']['bankName'],
                        'borrower': loan.borrower.id,
                        'loan': loan.id
                    }
                    data.append(datas)
                return Response(results, status=status.HTTP_200_OK)
        else:
            url = 'https://api.touchcore.tech/v1/remita/get-payment-history'
            headers = {
                "Authorization": "Bearer sk_f603f240ba3602994c03a3604670a01c17b857c9",
                "Content-Type": "application/json",
                'HOST': "api.touchcore.tech",
                'User-Agent': "RMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
                'Accept': "*/*"
            }
            datux = {
                "phone_number": borrower_mobile,
                "authorisation_channel": "USSD"
            }
            x = requests.get(url, headers=headers, data=datux, params=datux)
            if x.status_code !=200 and not 422:
                return Response({'error': 'Error. Please try again.'}, status=status.HTTP_400_BAD_REQUEST)
            elif x.status_code == 422:
                results = x.json()
                return Response(results, status=status.HTTP_404_NOT_FOUND)
            else:
                remita = BorrowerRemita.objects.filter(loan=loan, borrower=loan.borrower)
                data1 = []
                data = []
                data2 = []
                loan_remita = BorrowerLoanRemita.objects.filter(loan=loan, borrower=loan.borrower)
                serializer2 = BorrowerLoanRemitaSerializer(loan_remita, many=True)
                serializer = BorrowerRemitaSerializer(remita, many=True)
                customerName = loan.borrower.first_name + ' ' + loan.borrower.last_name
                data = {
                    'salaryPaymentDetails': serializer.data,
                    'loanHistoryDetails': serializer2.data
                }
                return Response({'message':'success', 'customerName': customerName, 'data':data}, status=status.HTTP_200_OK)
    
    def post(self, request):
        loan_id = request.data.get('loan')
        borrower_mobile = request.data.get('borrower_mobile')
        check_loan = Loan.objects.filter(id=loan_id).exists()
        if check_loan == False:
            return Response({'not_found': 'No loan with such ID'}, status=status.HTTP_404_NOT_FOUND)
        loan = Loan.objects.get(id=loan_id)
        if borrower_mobile == None:
            borrower_mobile = loan.borrower.mobile
        url = 'https://api.touchcore.tech/v1/remita/get-payment-history'
        headers = {
            "Authorization": "Bearer sk_f603f240ba3602994c03a3604670a01c17b857c9",
            "Content-Type": "application/json",
            'HOST': "api.touchcore.tech",
            'User-Agent': "RMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
            'Accept': "*/*"
        }
        datum = {
            # "phone_number": "08154567478",
            "phone_number": borrower_mobile,
            "authorisation_channel": "USSD"
        }
        x = requests.get(url, headers=headers, data=datum, params=datum)
        if x.status_code == 422:
            Borrower.objects.filter(id=loan.borrower.id).update(has_remita=False)
            return Response({'not_found': 'Customer not found'}, status=status.HTTP_404_NOT_FOUND)
        if x.status_code !=200:
            return Response({'error': 'Error. Please try again.'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            results = x.json()
            Borrower.objects.filter(id=loan.borrower.id).update(has_remita=True)
            data = []
            for info in results['data']['salaryPaymentDetails']:
                if loan.branch.date_format == 'dd/mm/yyyy':
                    date_is = datetime.datetime.strptime(info['paymentDate'], '%d-%m-%Y %H:%M:%S.%f')
                    d_date = date_is
                elif loan.branch.date_format == 'mm/dd/yyyy':
                    date_is = datetime.datetime.strptime(info['paymentDate'], '%m-%d-%Y %H:%M:%S.%f')
                    d_date = date_is
                else:
                    date_is = datetime.datetime.strptime(info['paymentDate'], '%d-%m-%Y %H:%M:%S+%f')
                    d_date = date_is
                BorrowerRemita.objects.create(
                    salary_amount = info['amount'],
                    account_number = info['accountNumber'],
                    payment_date= d_date.date(),
                    bank_code = info['bankCode'],
                    bank_name = results['data']['bankName'],
                    borrower = loan.borrower,
                    loan = loan
                )
                datas = {
                    'salary_amount': info['amount'],
                    'account_number': info['accountNumber'],
                    'payment_date': info['paymentDate'],
                    'bank_code': info['bankCode'],
                    'bank_name': results['data']['bankName'],
                    'borrower': loan.borrower.id,
                    'loan': loan.id
                }
                data.append(datas)
            return Response({'success': data}, status=status.HTTP_200_OK)


class DisbursementNotification(APIView):
    def post(self, request):
        loan_id = request.data.get('loan')
        total_collection_amount = request.data.get('total_collection_amount')
        collection_amount = request.data.get('collection_amount')
        # loan_amount = request.data.get('loan_amount')
        # number_of_repayments = request.data.get('number_of_repayments')
        # date_of_disbursement = request.data.get('date_of_disbursement')
        date_of_collection = request.data.get('date_of_collection')
        # borrower_mobile = request.data.get('borrower_mobile')

        
        check_loan = Loan.objects.filter(id=loan_id).exists()
        if check_loan == False:
            return Response({'not_found': 'No loan with such ID'}, status=status.HTTP_404_NOT_FOUND)
        loan = Loan.objects.get(id=loan_id)
        loan_disbursement = LoanDisbursement.objects.get(loan=loan)
        remita = BorrowerRemita.objects.filter(loan=loan, borrower=loan.borrower).last()
        # if borrower_mobile == None:
        borrower_mobile = loan.borrower.mobile
        if loan.branch.date_format == 'dd/mm/yyyy':
            date_is = datetime.datetime.strftime(loan_disbursement.date_disbursed, '%d-%m-%Y')
            d_date = date_is
        elif loan.branch.date_format == 'mm/dd/yyyy':
            date_is = datetime.datetime.strftime(loan_disbursement.date_disbursed, '%m-%d-%Y')
            d_date = date_is
        else:
            date_is = datetime.datetime.strftime(loan_disbursement.date_disbursed, '%d-%m-%Y')
            d_date = date_is
        url = 'https://api.touchcore.tech/v1/remita/loan-disbursement-notification'
        headers = {
            "Authorization": "Bearer sk_f603f240ba3602994c03a3604670a01c17b857c9",
            "Content-Type": "application/json",
            'HOST': "api.touchcore.tech",
            'User-Agent': "RMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
            'Accept': "*/*"
        }
        datum = {
            # "phone_number": "08154567478",
            "phone_number": borrower_mobile,
            "account_number": remita.account_number,
            "bank_code": remita.bank_code,
            "authorisation_channel": "USSD",
            "total_collection_amount": total_collection_amount,
            "collection_amount": collection_amount,
            "loan_amount": loan.principal_amount,
            "number_of_repayments": loan.no_of_repayments,
            "date_of_disbursement": d_date,
            "date_of_collection": date_of_collection
        }
        x = requests.post(url, headers=headers, data=datum, params=datum)
        if x.status_code == 200:
            results = x.json()
            Loan.objects.filter(id=loan.id).update(authorization_code=results['data']['authorisationCode'], customer_remita_id=results['data']['customerId'], mandate_reference=results['data']['mandateReference'], remita_activated=True)
            LoanDisbursement.objects.filter(loan=loan).update(remita_activated=True)
            return Response(results)
        else:
            results = x.json()
            return Response(results)


class DisbursementNotificationStop(APIView):
    def post(self, request):
        loan_id = request.data.get('loan')
        # total_collection_amount = request.data.get('total_collection_amount')
        # collection_amount = request.data.get('collection_amount')
        # loan_amount = request.data.get('loan_amount')
        # number_of_repayments = request.data.get('number_of_repayments')
        # date_of_disbursement = request.data.get('date_of_disbursement')
        # date_of_collection = request.data.get('date_of_collection')
        # borrower_mobile = request.data.get('borrower_mobile')
        
        check_loan = Loan.objects.filter(id=loan_id).exists()
        if check_loan == False:
            return Response({'not_found': 'No loan with such ID'}, status=status.HTTP_404_NOT_FOUND)
        loan = Loan.objects.get(id=loan_id)
        loan_disbursement = LoanDisbursement.objects.get(loan=loan)
        remita = BorrowerRemita.objects.filter(loan=loan, borrower=loan.borrower).last()
        borrower_mobile = loan.borrower.mobile
        url = 'https://api.touchcore.tech/v1/remita/stop-loan-collection'
        headers = {
            "Authorization": "Bearer sk_f603f240ba3602994c03a3604670a01c17b857c9",
            "Content-Type": "application/json",
            'HOST': "api.touchcore.tech",
            'User-Agent': "RMozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
            'Accept': "*/*"
        }
        datum = {
            "phone_number": borrower_mobile,
            "account_number": remita.account_number,
            "bank_code": remita.bank_code,
            "authorisation_channel": "USSD",
            "authorisation_code": loan.authorization_code,
            "mandate_reference": loan.mandate_reference,
            'customer_id': loan.customer_remita_id
        }
        x = requests.post(url, headers=headers, data=datum, params=datum)
        Loan.objects.filter(id=loan.id).update(remita_activated=False)
        LoanDisbursement.objects.filter(loan=loan).update(remita_activated=False)
        results = x.json()
        return Response(results)
