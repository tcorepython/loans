import requests
import json
from loan_management_system import settings


def directDebit(auth_code, email, amount):
    url = "https://api.paystack.co/charge"
    headers = {
        'Authorization': 'Bearer '+ settings.PAYSTACK_SECRET,
        'Content-Type' : 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
        'cookie': 'J8JBNpPEVEjx3QA4zTpn'
    }
    datum = {
        "email": email,
        "amount": amount,
        "authorization_code": auth_code
    }
    x = requests.post(url, data=json.dumps(datum), headers=headers)
    results = x.json()
    return results
# print(directDebit("AUTH_nbd2sdkqkb","lexmill99@gmail.com","700000"))


# def directDebit(auth_code, email, amount, bank_code, account_number):
#     url = "https://api.paystack.co/charge"
#     headers = {
#         'Authorization': 'Bearer sk_test_9945ca82c06c0bec14606fd3d5c968aa27057577',
#         'Content-Type' : 'application/json',
#         'Accept': 'application/json',
#         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
#         'cookie': 'J8JBNpPEVEjx3QA4zTpn'
#     }
#     datum = {
#         "email": "naijataste@gmail.com",
#         "amount": 3000,
#         "authorization_code": auth_code,
#         "metadata": {
#             "custom_fields": [
#                 {
#                     "value": "Loanx",
#                     "display_name": "Loan Payment for",
#                     "variable_name": "loan_payment_for"
#                 }
#             ]
#         }
#         "bank": {
#             "code": "044",
#             "account_number": "0800379373"
#         }
#     }
#     x = requests.post(url, data=json.dumps(datum), headers=headers)
#     results = x.json()
#     return results
#print(directDebit("AUTH_nbd2sdkqkb","lexmill99@gmail.com","500000"))
