# Generated by Django 3.0.7 on 2020-07-15 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('savings_investments', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='savingsproduct',
            name='name',
            field=models.CharField(default='', max_length=125),
        ),
    ]
