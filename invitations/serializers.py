import json
import datetime
from datetime import timedelta
from rest_framework import serializers
from django.core.exceptions import ValidationError
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _
from django.views.generic.detail import SingleObjectMixin
from django.core.mail import EmailMessage

from django.conf import settings
from django.contrib.sites.models import Site
try:
    from django.urls import reverse
    from django.urls import reverse_lazy
except ImportError:
    from django.core.urlresolvers import reverse
    from django.urls.reverse_lazy import reverse_lazy
from .adapters import get_invitations_adapter, get_investor_invitations_adapter
from .app_settings import app_settings
from rest_framework.decorators import action
from .exceptions import AlreadyAccepted, AlreadyInvited, UserRegisteredEmail
from .forms import CleanEmailMixin
from .signals import invite_accepted
from .utils import get_invitation_model, get_investor_invitation_model, get_invite_form
from . import signals

from django.utils import timezone
from django.utils.crypto import get_random_string

from .models import Invitation, InvestorInvitation
from borrowers.models import *
from investors.models import *
from accounts.models import *
Invitation = get_invitation_model()
InvestorInvitation = get_investor_invitation_model()
key = get_random_string(64).lower()


class SendInviteSerializer(serializers.ModelSerializer):
	key = serializers.HiddenField(default=get_random_string(64).lower())
	class Meta:
		model = Invitation
		fields = ['email_address', 'key']


	@classmethod
	def create(self, validated_data):
		instance = Invitation.objects.create(**validated_data)
		current_site = validated_data.pop('site', Site.objects.get_current())
		if instance:
			invite_url = reverse_lazy('accounts-list',
	                            args=[instance.key])
			ctx = validated_data
			check_mail = Borrower.objects.filter(email=ctx['email_address']).exists()
			if check_mail:
				chk_mail = Borrower.objects.get(email=ctx['email_address'])
				InviteBorrower.objects.create(email_address=chk_mail)
				print('User is Already a Borrower')
			else:
				InviteBorrower.objects.create(email_address=ctx['email_address'])
				print('User is not a Borrower')
			ctx.update({
				'invite_url': invite_url,
	            'site_name': current_site.name,
	            'domain': current_site.domain,
	            'email_address': validated_data['email_address'],
	            'key': validated_data['key'],
	            })
			email_template = 'invitations/email/email_invite'

			get_invitations_adapter().send_mail(
	            email_template,
	            instance.email_address,
	            ctx)
			instance.sent = timezone.now()
			instance.save()

			signals.invite_url_sent.send(
	            sender=instance.__class__,
	            instance=instance,
	            invite_url_sent=invite_url)
		return instance

	def key_expired(self):
		expiration_date = (
			self.sent + datetime.timedelta(
				days=app_settings.INVITATION_EXPIRY))
		return expiration_date <= timezone.now()


class SendInvestorInviteSerializer(serializers.ModelSerializer):
	key = serializers.HiddenField(default=get_random_string(64).lower())
	class Meta:
		model = InvestorInvitation
		fields = ['email_address', 'key']


	@classmethod
	def create(self, validated_data):
		instance = InvestorInvitation.objects.create(**validated_data)
		current_site = validated_data.pop('site', Site.objects.get_current())
		if instance:
			invite_url = reverse_lazy('accounts-list',
	                            args=[instance.key])
			ctx = validated_data
			# Let's remove everything after @
			the_email = ctx['email_address']
			split_email, remove_sysmob, remove_sub = the_email.partition('@')
			check_mail = Investor.objects.filter(email=ctx['email_address']).exists()
			if check_mail:
				chk_mail = Investor.objects.get(email=ctx['email_address'])
				InvestorInvite.objects.create(email_address=chk_mail)
				print('User is Already an Investor')
			else:
				InvestorInvite.objects.create(email_address=ctx['email_address'])
				print('User is not an Investor')
			ctx.update({
				'invite_url': invite_url,
	            'site_name': current_site.name,
	            'domain': current_site.domain,
	            'investor_name': split_email.capitalize(),
	            'email_address': validated_data['email_address'],
	            'key': validated_data['key'],
	            })
			email_template = 'invitations/email/email_investor_invite'

			get_invitations_adapter().send_mail(
	            email_template,
	            instance.email_address,
	            ctx)
			instance.sent = timezone.now()
			instance.save()

			signals.invite_url_sent.send(
	            sender=instance.__class__,
	            instance=instance,
	            invite_url_sent=invite_url)
		return instance

	def key_expired(self):
		expiration_date = (
			self.sent + datetime.timedelta(
				days=app_settings.INVITATION_EXPIRY))
		return expiration_date <= timezone.now()
