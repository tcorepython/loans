from rest_framework import serializers
from .models import *
from django.core.mail import EmailMessage, send_mail
from loan_management_system import settings
import random
import string
from datetime import datetime
import datetime
import requests
from loans.serializers import LoanOfficerSerializer
from loans.models import Loan
import json


def random_ref_generator(size=20, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

ref_gen = random_ref_generator()


def create_ref_code():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))


class BorrowerSerializer(serializers.ModelSerializer):
    loan_officer = serializers.SerializerMethodField()
    # date_of_birth = serializers.SerializerMethodField()
    existing_loans = serializers.SerializerMethodField()
    total_paid_loans = serializers.SerializerMethodField()

    class Meta:
        model = Borrower
        read_only_fields = ('is_activated',)
        fields = '__all__'

    def get_loan_officer(self, obj):
        if obj.loan_officer:
            name = obj.loan_officer.staff_id.user_id.user.first_name + ' ' + str(obj.loan_officer.staff_id.user_id.user.last_name)
            return name

    # def get_date_of_birth(self, obj):
    #     if obj.date_of_birth:
    #         if obj.profile.profile.branch.date_format == 'dd/mm/yyyy':
    #             date_is = obj.date_of_birth.strftime('%d-%m-%Y')
    #             return date_is
    #         elif obj.profile.profile.branch.date_format == 'mm/dd/yyyy':
    #             date_is = obj.date_of_birth.strftime('%m-%d-%Y')
    #             return date_is
    #         else:
    #             date_is = obj.date_of_birth.strftime('%Y-%m-%d')
    #             return date_is

    def get_existing_loans(self, obj):
        counts = Loan.objects.all().filter(borrower=obj.id).count()
        return counts

    def get_total_paid_loans(self, obj):
        counts = Loan.objects.all().filter(borrower=obj.id).filter(status="fully paid").count()
        return counts


class BorrowerSerializer2(serializers.ModelSerializer):
    class Meta:
        model = Borrower
        read_only_fields = ('is_activated',)
        fields = '__all__'


class BorrowerFileSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Borrower_File


class MembershipSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    # group_name = serializers.SerializerMethodField()
    # members = serializers.PrimaryKeyRelatedField(queryset=Membership.objects.all(), many=True)
    class Meta:
        model = Membership
        fields = ('__all__')
        read_only_fields = ('name',)

    def get_name(self, obj):
        name = obj.borrower.first_name + ' ' + str(obj.borrower.last_name)
        return name

class BorrowerGroupSerializer(serializers.ModelSerializer):
    members = BorrowerSerializer(many=True)
    group_leader = serializers.SerializerMethodField()

    class Meta:
        model = BorrowerGroup
        fields = '__all__'
        read_only_fields = ('members',)

    def get_group_leader(self, obj):
        if obj.group_leader:
            name = obj.group_leader.first_name + ' ' + obj.group_leader.last_name
            return name


class BorrowerGroupSerializer2(serializers.ModelSerializer):
    class Meta:
        model = BorrowerGroup
        fields = '__all__'


class InviteBorrowerSerializer(serializers.ModelSerializer):
    class Meta:
        model = InviteBorrower
        fields = '__all__'

class PaystackSerializer(serializers.ModelSerializer):
    # paystack_charge_id = serializers.CharField(initial=ref_gen, required=False)
    class Meta:
        model = PaystackTransaction
        fields = '__all__'

    @classmethod
    def perform_create(self, validated_data):
        instance = PaystackTransaction.objects.create(**validated_data)

        return instance


class BorrowerRemitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = BorrowerRemita
        fields = '__all__'


class BorrowerLoanRemitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = BorrowerLoanRemita
        fields = '__all__'
